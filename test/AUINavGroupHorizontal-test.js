import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import AUINavGroupHorizontal from '../src/AUINavGroupHorizontal';

describe('AUINavGroupHorizontal', () => {
    it('should render the correct AUI markup', () => {
        expect(shallow(<AUINavGroupHorizontal />).html()).to.equal(`<nav class="aui-navgroup aui-navgroup-horizontal"><div class="aui-navgroup-inner"><div class="aui-navgroup-primary"></div><div class="aui-navgroup-secondary"></div></div></nav>`);
    });
});
