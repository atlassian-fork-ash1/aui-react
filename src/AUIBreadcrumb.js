import React from 'react';
import PropTypes from 'prop-types';

const AUIBreadcrumb = ({ children }) => (
    <li>
        <a href="#">
            <span className="aui-nav-item-label">{children}</span>
        </a>
    </li>
);

AUIBreadcrumb.propTypes = {
    children: PropTypes.node
};

export default AUIBreadcrumb;
