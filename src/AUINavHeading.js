import React from 'react';
import PropTypes from 'prop-types';

const AUINavHeading = ({ children }) => (
    <div className="aui-nav-heading"><strong>{children}</strong></div>
);

AUINavHeading.propTypes = {
    children: PropTypes.node
};

export default AUINavHeading;
