/* istanbul ignore next */
export function addEventListener() {
    if (typeof window !== 'undefined') {
        window.addEventListener.apply(window, arguments);
    }
}

/* istanbul ignore next */
export function removeEventListener() {
    if (typeof window !== 'undefined') {
        window.removeEventListener.apply(window, arguments);
    }
}
