import React from 'react';
import PropTypes from 'prop-types';

const AUIBanner = ({ children }) => (
    <div className="aui-banner aui-banner-error" role="banner" aria-hidden="false">
        {children}
    </div>
);

AUIBanner.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.array
    ])
};

export default AUIBanner;
