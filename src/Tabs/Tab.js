import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';

export default class Tab extends PureComponent {
  render() {
    const { id, isActive, className, children, ...props } = this.props;

    delete props.label;

    const paneClassName = classNames('tabs-pane', className, {
      'active-pane': isActive
    });

    return (
      <div className={paneClassName} id={id} {...props}>
        {children}
      </div>
    );
  }
}

Tab.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.node.isRequired,
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    isActive: PropTypes.bool
};

Tab.defaultProps = {
    className: null,
    isActive: false
};
